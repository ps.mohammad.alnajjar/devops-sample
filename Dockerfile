FROM openjdk:8

COPY ./target/assig*.jar /usr/app/

ENV SPRING_PROFILE_ACTIVE=mysql

WORKDIR /usr/app/

EXPOSE 8090

ENTRYPOINT java -jar  -Dspring.profiles.active=${SPRING_PROFILE_ACTIVE} assignment*.jar